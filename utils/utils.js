import axios from "axios";
import { message } from "antd";

const Notification = (content = "", type = "info", duration = 2) => {
  message.open({
    content: content,
    type: type,
    duration: duration,
  });
};

const GeneralHTTPRequest = async (request, callback, error) => {
  request
    .then((response) => {
      callback(response.data);
    })
    .catch((e) => {
      Notification(e.message, "warning");
      error(e);
    });
};

const GeneralFetch = (url, method, data) => {
  return axios({
    method: method,
    url: url,
    data: data,
  });
};

const fetcher = async (url) => {
  const res = await fetch(url);
  if (!res.ok) {
    const error = new Error("An error occurred while fetching the data.");
    error.info = await res.json();
    error.status = res.status;
    throw error;
  }
  return res.json();
};

const fetcherOptions = {
  shouldRetryOnError: false,
};

const isFunction = (functionToCheck) => {
  return (
    functionToCheck && {}.toString.call(functionToCheck) === "[object Function]"
  );
};

export {
  Notification,
  GeneralHTTPRequest,
  GeneralFetch,
  fetcher,
  fetcherOptions,
};
