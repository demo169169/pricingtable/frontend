import { useState, useEffect } from "react";
import { mutate } from "swr";
import {
  Button,
  Dropdown,
  Form,
  Input,
  InputNumber,
  Menu,
  Modal,
  Select,
} from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFolder,
  faTable,
  faPlus,
  faPen,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { GeneralHTTPRequest } from "../utils/utils";
import { createItem, deleteItem, updateItem } from "../services/items";
import { createPlan, deletePlan, updatePlan } from "../services/plans";

const { Option } = Select;
const { SubMenu } = Menu;

const title = [
  `Type - Add`,
  `Type - Edit`,
  `Type - Delete`,
  `Plan - Add`,
  `Plan - Edit`,
  `Plan - Delete`,
];

const ActionMenu = ({ items = [], plans = [], modalConfig }) => {
  const [form] = Form.useForm();

  const [showModal, showModalSet] = useState(false);
  // 0 Type 00 Add 01 Edit 02 Detele
  // 1 Plan 10 Add 11 Edit 12 Detele
  const [modalType, modalTypeSet] = useState(0);
  const [name, nameSet] = useState(
    `TEST${Math.floor(Math.random() * 100) + 1}`
  );
  const [selectedItems, selectedItemsSet] = useState([]);
  const [price, priceSet] = useState(10);
  const [selectedPlan, selectedPlanSet] = useState();
  const [selectedItem, selectedItemSet] = useState();
  const handleShowContent = () => {
    let component;
    switch (modalType) {
      case 0:
      case 1:
      case 2:
        component = (
          <>
            {(modalType === 1 || modalType === 2) && (
              <Form.Item
                name="item_id"
                label="Items"
                rules={[{ required: true }]}
              >
                <Select
                  style={{ width: "100%" }}
                  placeholder="Items"
                  value={selectedItem}
                  onChange={(value) => selectedItemSet(value)}
                >
                  {handleShowOption("items")}
                </Select>
              </Form.Item>
            )}
            {(modalType === 0 || modalType === 1) && (
              <>
                <Form.Item
                  name="name"
                  label="Name"
                  initialValue={name}
                  rules={[{ required: true }]}
                >
                  <Input
                    value={name}
                    onChange={(e) => nameSet(e.target.value)}
                  />
                </Form.Item>
              </>
            )}
          </>
        );
        break;
      case 3:
      case 4:
      case 5:
        component = (
          <>
            {(modalType === 4 || modalType === 5) && (
              <Form.Item
                name="plan_id"
                label="Plan"
                rules={[{ required: true }]}
              >
                <Select
                  style={{ width: "100%" }}
                  placeholder="Plan"
                  value={selectedPlan}
                  onChange={(value) => selectedPlanSet(value)}
                >
                  {handleShowOption("plans")}
                </Select>
              </Form.Item>
            )}
            {(modalType === 3 || modalType === 4) && (
              <>
                <Form.Item
                  name="name"
                  label="Name"
                  initialValue={name}
                  rules={[{ required: true }]}
                >
                  <Input
                    value={name}
                    onChange={(e) => nameSet(e.target.value)}
                  />
                </Form.Item>
                <Form.Item
                  name="selectedItems"
                  label="Types"
                  initialValue={selectedItems}
                  rules={[{ required: true }]}
                >
                  <Select
                    mode="tags"
                    style={{ width: "100%" }}
                    value={selectedItems}
                    onChange={(value) => selectedItemsSet(value)}
                  >
                    {handleShowOption("items")}
                  </Select>
                </Form.Item>
                <Form.Item
                  name="price"
                  label="Price"
                  initialValue={price}
                  rules={[{ required: true, type: "number" }]}
                >
                  <InputNumber
                    value={price}
                    onChange={(value) => priceSet(value)}
                  />
                </Form.Item>
              </>
            )}
          </>
        );
    }
    return component;
  };
  const handleShowOption = (type) => {
    switch (type) {
      case "items":
        return items.map((item, index) => {
          return (
            <Option key={index} value={item.item_id}>
              {item.name}
            </Option>
          );
        });
      case "plans":
        return plans.map((plan, index) => {
          return (
            <Option key={index} value={plan.plan_id}>
              {plan.name}
            </Option>
          );
        });
    }
  };
  const handleShowModal = (type) => {
    if (type !== undefined) modalTypeSet(type);
    showModalSet(!showModal);
  };
  const onOk = async () => {
    console.log("OOO WHY OK");
    form.validateFields().then(async (values) => {
      let data = {
        name: values.name,
        items: values.selectedItems,
        price: values.price,
      };
      let request = [
        createItem,
        updateItem,
        deleteItem,
        createPlan,
        updatePlan,
        deletePlan,
      ];
      let params = {};
      let callback = () => {
        mutate(`/api/items`);
        mutate(`/api/plans`);
      };
      switch (modalType) {
        case 0:
          params = [data];
          break;
        case 1:
          params = [data, values.item_id];
          break;
        case 2:
          params = [values.item_id];
          break;
        case 3:
          params = [data];
          break;
        case 4:
          params = [data, values.plan_id];
          break;
        case 5:
          params = [values.plan_id];
          break;
      }
      handleGeneralHTTPRequest(request[modalType], params, callback);
      onCancel();
    });
  };
  const onCancel = () => {
    handleShowModal();
    form.resetFields();
  };
  const handleGeneralHTTPRequest = (request, data, callback) => {
    GeneralHTTPRequest(
      request(...data),
      (response) => {
        callback();
      },
      (error) => {}
    );
  };
  useEffect(() => {
    if (selectedItem) {
      switch (modalType) {
        case 1:
          const item = items.find((item) => item.item_id === selectedItem);
          form.setFieldsValue({
            name: item.name,
          });
          break;
      }
    }
  }, [selectedItem]);
  useEffect(() => {
    if (selectedPlan) {
      switch (modalType) {
        case 4:
          const plan = plans.find((plan) => plan.plan_id === selectedPlan);
          form.setFieldsValue({
            name: plan.name,
            selectedItems: plan.items,
            price: plan.price,
          });
          break;
      }
    }
  }, [selectedPlan]);
  return (
    <>
      <Dropdown
        overlay={
          <Menu>
            <SubMenu
              key={10}
              title={
                <>
                  <FontAwesomeIcon icon={faFolder} /> Type
                </>
              }
            >
              <Menu.Item key={0} onClick={() => handleShowModal(0)}>
                <FontAwesomeIcon icon={faPlus} /> Add
              </Menu.Item>
              <Menu.Item key={1} onClick={() => handleShowModal(1)}>
                <FontAwesomeIcon icon={faPen} /> Edit
              </Menu.Item>
              <Menu.Item key={2} onClick={() => handleShowModal(2)}>
                <FontAwesomeIcon icon={faTrash} /> Delete
              </Menu.Item>
            </SubMenu>
            <SubMenu
              key={11}
              title={
                <>
                  <FontAwesomeIcon icon={faTable} /> Plan
                </>
              }
            >
              <Menu.Item key={3} onClick={() => handleShowModal(3)}>
                <FontAwesomeIcon icon={faPlus} /> Add
              </Menu.Item>
              <Menu.Item key={4} onClick={() => handleShowModal(4)}>
                <FontAwesomeIcon icon={faPen} /> Edit
              </Menu.Item>
              <Menu.Item key={5} onClick={() => handleShowModal(5)}>
                <FontAwesomeIcon icon={faTrash} /> Delete
              </Menu.Item>
            </SubMenu>
          </Menu>
        }
        placement="bottomCenter"
        arrow
      >
        <Button type="text">Action</Button>
      </Dropdown>
      <Modal
        {...modalConfig}
        title={title[modalType]}
        centered={true}
        visible={showModal}
        onOk={onOk}
        onCancel={onCancel}
      >
        <Form
          form={form}
          name={title[modalType]}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          onFinish={() => {}}
          onFinishFailed={() => {}}
        >
          {handleShowContent()}
        </Form>
      </Modal>
    </>
  );
};

export default ActionMenu;
