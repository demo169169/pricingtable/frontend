import Head from "next/head";
import useSWR from "swr";
import { useEffect, useState } from "react";
import { Card, Col, Empty, Radio, Row, Select, Spin, Typography } from "antd";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCheck } from "@fortawesome/free-solid-svg-icons";
import { Notification, fetcher, fetcherOptions } from "../utils/utils";
import ActionMenu from "../components/menu";
import classNames from "classnames";
import styles from "../styles/Home.module.css";

const { Option } = Select;
const { Text } = Typography;

const Home = () => {
  const {
    data: itemsData,
    error: itemsError,
    isValidating: itemsLoading,
  } = useSWR("/api/items", fetcher, fetcherOptions);
  const {
    data: plansData,
    error: plansError,
    isValidating: plansLoading,
  } = useSWR("/api/plans", fetcher, fetcherOptions);
  const [items, itemsSet] = useState([]);
  const [plans, plansSet] = useState([]);
  const [displayPlans, displayPlansSet] = useState([]);
  const [selectedPlan, selectedPlanSet] = useState("");
  const handleSelectChange = (value) => {
    if (value.length === 1) {
      Notification("At least two plans are selected to compare.", "warning");
    } else {
      displayPlansSet(value);
    }
  };
  const handleSort = (a, b) => {
    return a.price > b.price ? 1 : -1;
  };
  const handleFilter = (plan) => {
    return displayPlans.indexOf(plan.plan_id) > -1;
  };
  useEffect(() => {
    const items = itemsData?.data || [];
    itemsSet(items);
  }, [itemsData]);
  useEffect(() => {
    const plans = plansData?.data || [];
    plansSet(plans);
  }, [plansData]);
  useEffect(() => {
    if (itemsError || plansError) {
      Notification("Something goes wrong!", "error");
    }
  }, [itemsError, plansError]);
  useEffect(() => {
    displayPlansSet(plans.map((plan) => plan.plan_id));
  }, [plans]);
  return (
    <>
      <Head>
        <title>Pricing Demo</title>
      </Head>
      <Row className={styles.container} justify="center">
        <Col xs={24} sm={24} md={24} lg={14} xl={14} xxl={12}>
          <Card
            className={classNames(styles.card)}
            title={
              <Row>
                <Col>
                  <ActionMenu items={items} plans={plans} />
                </Col>
              </Row>
            }
          >
            <Spin spinning={itemsLoading || plansLoading}>
              <Row gutter={[8, 8]}>
                <Col span={24}>
                  <Row>
                    <Col span={24}>
                      <Text strong>Choose a plan</Text>
                    </Col>
                    <Col span={24}>You can compare by selecting the plan</Col>
                  </Row>
                </Col>
                <Col span={24}>
                  <Select
                    mode="tags"
                    style={{ width: "100%" }}
                    placeholder="Plans"
                    value={displayPlans}
                    onChange={handleSelectChange}
                  >
                    {plans.map((plan, index) => {
                      return (
                        <Option key={index} value={plan.plan_id}>
                          {plan.name}
                        </Option>
                      );
                    })}
                  </Select>
                </Col>
                <Col span={24}>
                  {items.length > 0 && plans.length > 0 ? (
                    <TableContainer>
                      <Table size="small">
                        <TableHead>
                          <TableRow>
                            <TableCell
                              className={classNames(styles.stickyCol)}
                            ></TableCell>
                            {plans
                              // .sort(handleSort)
                              .filter(handleFilter)
                              .map((plan, index) => {
                                return (
                                  <TableCell key={index} align="center">
                                    {plan.name}
                                  </TableCell>
                                );
                              })}
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {items.map((item, index) => {
                            return (
                              <TableRow key={index} hover>
                                <TableCell
                                  className={classNames(styles.stickyCol)}
                                >
                                  {item.name}
                                </TableCell>
                                {plans
                                  // .sort(handleSort)
                                  .filter(handleFilter)
                                  .map((plan, key) => {
                                    const include = plan.items.find(
                                      (x) => x === item.item_id
                                    );
                                    return (
                                      <TableCell key={key} align="center">
                                        <FontAwesomeIcon
                                          icon={include ? faCheck : faTimes}
                                          color={include ? "green" : "red"}
                                        />
                                      </TableCell>
                                    );
                                  })}
                              </TableRow>
                            );
                          })}
                        </TableBody>
                        <TableFooter>
                          <TableRow>
                            <TableCell
                              className={classNames(styles.stickyCol)}
                            ></TableCell>
                            {plans
                              // .sort(handleSort)
                              .filter(handleFilter)
                              .map((plan, index) => {
                                return (
                                  <TableCell key={index} align="center">
                                    <Radio
                                      checked={selectedPlan === plan.plan_id}
                                      value={plan.plan_id}
                                      onClick={() =>
                                        selectedPlanSet(plan.plan_id)
                                      }
                                    >
                                      ${plan.price}
                                    </Radio>
                                  </TableCell>
                                );
                              })}
                          </TableRow>
                        </TableFooter>
                      </Table>
                    </TableContainer>
                  ) : (
                    <Empty />
                  )}
                </Col>
              </Row>
            </Spin>
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default Home;
