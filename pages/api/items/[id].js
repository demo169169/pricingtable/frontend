import axios from "axios";

export default async function handler(req, res) {
  await axios(`${process.env.APP_API}/items/${req.query.id}`, {
    headers: {
      "content-type": "application/json",
    },
    method: req.method,
    data: req.body,
  })
    .then((response) => {
      res.status(200).json(response.data);
    })
    .catch((error) => {
      res.status(400).json(error);
    });
}
