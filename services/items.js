import { GeneralFetch } from "../utils/utils";

const createItem = (data) => {
  return GeneralFetch("/api/items", "POST", data);
};

const updateItem = (data, id) => {
  return GeneralFetch(`/api/items/${id}`, "PUT", data);
};

const deleteItem = (id) => {
  return GeneralFetch(`/api/items/${id}`, "DELETE");
};

export { createItem, updateItem, deleteItem };