import { GeneralFetch } from "../utils/utils";

const createPlan = (data) => {
  return GeneralFetch("/api/plans", "POST", data);
};

const updatePlan = (data, id) => {
  return GeneralFetch(`/api/plans/${id}`, "PUT", data);
};

const deletePlan = (id) => {
  return GeneralFetch(`/api/plans/${id}`, "DELETE");
};

export { createPlan, updatePlan, deletePlan };
