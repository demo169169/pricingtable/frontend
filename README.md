<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/pUKopWQ.png" alt="Project logo"></a>
</p>

<h3 align="center">Pricing Table</h3>

---

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

A pricing table frontend with Restful API using NextJs with ReactJs

## 👀 Demo <a name = "demo"></a>

http://demo.davidchiu.work

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### 🧰 Prerequisites

```
- Node
- Yarn (Optional but preferable)
```

### ⚙️ Installing

```
1. git clone git@gitlab.com:demo169169/pricingtable/frontend.git
2. cd frontend
3. yarn install
4. yarn dev
```

## 🔧 Running the tests <a name = "tests"></a>

```
- yarn test
```

## 🎈 Usage <a name="usage"></a>

```
- Select any plan in the dropdown to compare
- Explore "Action" button to add/edit/delete type or plan
```

## 🚀 Deployment <a name = "deployment"></a>

```
1. change the APP_API in Dockerfile to your pricing backend docker's IP
2. docker build . -t pricing-frontend
3. docker run -p 80:3000 pricing-frontend
```

## ⛏️ Built Using <a name = "built_using"></a>

- [ReactJs](https://reactjs.org/) - Web Framework
- [NextJs](https://nextjs.org/) - ReactJs Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment

## ✍️ Authors <a name = "authors"></a>

- [@chingchiu169](https://gitlab.com/chingchiu169)

## 🎉 Acknowledgements <a name = "acknowledgement"></a>
```
- Enjoy coding
```