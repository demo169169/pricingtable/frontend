import { shallow } from "enzyme";
import React from "react";

import App from "../pages/index";

describe("Render Index", () => {
  it('App shows "Pricing Demo" in a <title> tag', () => {
    const app = shallow(<App />);
    expect(app.find("title").text()).toEqual("Pricing Demo");
  });
});
